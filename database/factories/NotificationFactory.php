<?php

namespace Database\Factories;

use App\Models\EventGroup;
use App\Models\Notification;
use Illuminate\Database\Eloquent\Factories\Factory;

class NotificationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Notification::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'message' => $this->faker->paragraph,
            'is_read' => $this->faker->boolean,
            'created_at' => $this->faker->dateTimeBetween('-7 days')
        ];
    }

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterMaking(function (Notification $notification) {
            //
        })->afterCreating(function (Notification $notification) {
            $notification->update(['event_group_id' => EventGroup::all()->random()->id]);
        });
    }

}

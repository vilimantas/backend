<?php

namespace Database\Seeders;

use App\Models\EventGroup;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        EventGroup::factory()
            ->count(7)
            ->create();

         User::factory()
             ->count(1)
             ->has(Notification::factory()->count(50))
             ->create();
    }
}

<?php


namespace App\Repositories;


use App\Models\User;

class UserRepository
{
    public function storeUser(array $userData) : User
    {
        return User::create($userData);
    }
}

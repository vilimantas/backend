<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Repositories\UserRepository;
use Illuminate\Http\Response;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param RegisterRequest $request
     * @param UserRepository $repository
     * @return Response
     */
    public function __invoke(RegisterRequest $request, UserRepository $repository) : Response
    {
        $data = $request->validated();
        $user = $repository->storeUser($data);
        return response($user);
    }
}

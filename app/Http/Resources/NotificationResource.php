<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'message' => $this->when($request->full_message, $this->message),
            'short_message' => Str::limit($this->message, 50),
            'event_group' => new EventGroupResource($this->eventGroup),
            'is_read' => $this->is_read,
            'created_at' => $this->created_at->diffForHumans()
        ];
    }
}

<?php

use App\Http\Resources\NotificationResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//ENDPOINT FOR FRONTED TESTING
Route::get('/notifications', function (Request $request) {

    $user = \App\Models\User::find(1);

    $notifications = $user->notifications()->with('eventGroup');
    $new = (clone $notifications)->where('is_read', false)->count();

    return NotificationResource::collection(
        $notifications
            ->orderBy('is_read')
            ->orderByDesc('created_at')
            ->paginate($request->input('perPage', 5))
    )->additional(['meta' => [
        'new' => $new
    ]]);
});
